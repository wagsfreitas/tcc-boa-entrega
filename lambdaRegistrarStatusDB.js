const AWS_DB = require('aws-sdk');
const ddb = new AWS_DB.DynamoDB.DocumentClient({region: 'us-east-2'});

exports.handler = async (event, context, callback) => {
    const trackId = event.Records[0].messageAttributes.trackId.stringValue;
    const mensagemSqs = event.Records[0].body;
    var rotaDefinida = new Array();
    
    //leitura do banco
    await readMessage(trackId).then(data => {
    data.Items.forEach(function(item, element) {
        rotaDefinida = item.Rota;
    });
    }).catch((err) => {
        console.error(err);
    })
    

    rotaDefinida.push(mensagemSqs);
    
    //escrita banco
    await createMessage(trackId, rotaDefinida).then(() => {
    }).catch((err) => {
        console.error(err)
    })
};


function createMessage(requestId, rota) {
    const params = {
        TableName: 'StatusEntrega',
        Item: {
            'TrackId': requestId,
            'Rota': rota
        }
    }
    console.log('Params: ', params);
    return ddb.put(params).promise();
}


function readMessage(trackId) {
    const params = {
        ExpressionAttributeValues: {
           ":v1":  trackId
          }, 
        FilterExpression: 'TrackId = :v1',
        TableName: 'StatusEntrega',
        Limit: 10
    }
    return ddb.scan(params).promise();
}
