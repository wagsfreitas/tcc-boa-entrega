const AWS_DB = require('aws-sdk');
const ddb = new AWS_DB.DynamoDB.DocumentClient({region: 'us-east-2'});

exports.handler = async (event, context, callback) => {
    var trackId = event.queryStringParameters.trackId;
    await readMessage(trackId).then(data => {
        var rota;
        var httpCode = 200
        var items = data.Items;
        if(items.length > 0) {
        rota = items[0].Rota;
        }
        
        callback(null, {
            "isBase64Encoded": false,
            "statusCode": httpCode,
            "body": JSON.stringify(rota),
            "headers": {
            },
        })
    }).catch((err) => {
        console.error(err);
    })
};

// Consulta no dynamo
function readMessage(trackId) {
    const params = {
        ExpressionAttributeValues: {
           ":v1":  trackId
          }, 
        FilterExpression: 'TrackId = :v1',
        TableName: 'StatusEntrega',
        Limit: 10
    }
    return ddb.scan(params).promise();
}
