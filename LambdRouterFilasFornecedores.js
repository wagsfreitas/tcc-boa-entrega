const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({region: 'us-east-2'});

exports.handler = async (event, context, callback) => {
    var idFornecedor = parseInt(event.queryStringParameters.idFornecedor);
    var trackId = event.queryStringParameters.trackId;
    var body = event.body;

    await readConfig(idFornecedor).then(data => {
        var items = data.Items;  
        var urlFila = items[0].urlFila;
        sendMessage(urlFila, trackId, body);
        const retorno = items
        callback(null, {
            "isBase64Encoded": false,
            "statusCode": 201,
            "headers": {
            },
        })
    }).catch((err) => {
        console.error(err);
    })
};

// Consulta no dynamo
function readConfig(idFornecedor) {
    const params = {
        ExpressionAttributeValues: {
           ":v1":  idFornecedor
          }, 
        FilterExpression: 'id = :v1',
        TableName: 'FilasFornecedores',
        Limit: 10
    }
    return ddb.scan(params).promise();
}

// Envio fila
function sendMessage(url, trackId, body) {
    var sqs = new AWS.SQS();
    const params = {
        QueueUrl: url,
        MessageGroupId: '1',
        MessageDeduplicationId: trackId,
        MessageAttributes: {
            'trackId': {
                DataType: "String",
                StringValue: trackId
            }
        },
        MessageBody: body
    }
    sqs.sendMessage(params, function(err, data) {
        if (err) console.log(err, err.stack); // logar erro envio de mensagem
    });
    return ;
}
